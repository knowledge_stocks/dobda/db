/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `member` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `pwd` varbinary(64) NOT NULL,
  `reg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bank_refresh_token` varchar(400) COLLATE utf8mb4_general_ci NOT NULL,
  `bank_token` varchar(400) COLLATE utf8mb4_general_ci NOT NULL,
  `bank_token_expire_time` datetime NOT NULL,
  `bank_user_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `deny_time` datetime DEFAULT NULL,
  `introduction` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `level` tinyint unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `nickname` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `phone_num` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `total_received_report_count` int unsigned NOT NULL DEFAULT '0',
  `total_request_count` int unsigned NOT NULL DEFAULT '0',
  `trust_point` double unsigned NOT NULL DEFAULT '36.5',
  `unread_noti_count` tinyint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mbmcqelty0fbrvxp1q58dn57t` (`email`),
  UNIQUE KEY `UK_hh9kg6jti4n1eoiertn2k6qsc` (`nickname`),
  UNIQUE KEY `UK_qbmf4romr5dmx3lytr1kbv82w` (`phone_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
