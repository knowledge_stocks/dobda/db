/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `order` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `amount` int unsigned NOT NULL,
  `pay_method` tinyint unsigned NOT NULL,
  `reg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` tinyint unsigned NOT NULL,
  `helper_id` int unsigned DEFAULT NULL,
  `requester_id` int unsigned DEFAULT NULL,
  `request_id` bigint unsigned NOT NULL,
  `requester_fintech` varchar(24) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `requester_bank_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8gtlv2ron6x3go4ledaw8hdkx` (`helper_id`),
  KEY `FK7xd84elfnd3ueas494q50g8vb` (`requester_id`),
  KEY `FKqv0ri81otghyrbliqynbynlrt` (`request_id`),
  CONSTRAINT `FK7xd84elfnd3ueas494q50g8vb` FOREIGN KEY (`requester_id`) REFERENCES `member` (`id`),
  CONSTRAINT `FK8gtlv2ron6x3go4ledaw8hdkx` FOREIGN KEY (`helper_id`) REFERENCES `member_helper` (`id`),
  CONSTRAINT `FKqv0ri81otghyrbliqynbynlrt` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
