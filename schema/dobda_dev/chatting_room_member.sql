/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `chatting_room_member` (
  `room_id` bigint unsigned NOT NULL,
  `member_id` int unsigned NOT NULL,
  `has_unread_msg` tinyint(1) NOT NULL DEFAULT '0',
  `is_hide` tinyint(1) NOT NULL DEFAULT '0',
  `partner_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`room_id`,`member_id`),
  KEY `FKatbj5qw57fkw5xheqf9hhgdn` (`member_id`),
  KEY `FK3fv5e7vomsadpurxim1619wu4` (`partner_id`),
  CONSTRAINT `FK3fv5e7vomsadpurxim1619wu4` FOREIGN KEY (`partner_id`) REFERENCES `member` (`id`),
  CONSTRAINT `FKatbj5qw57fkw5xheqf9hhgdn` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKs0aor38donve17f7r75mgo98v` FOREIGN KEY (`room_id`) REFERENCES `chatting_room` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
